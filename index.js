

fetch(`https://jsonplaceholder.typicode.com/posts`)
.then(res => res.json())
.then(result => {
    showPosts(result);
    console.log(result)
})




const showPosts = (posts)=>{
    let entries = '';

    posts.forEach( (post, index)=>{
        entries +=
       ` <div class ="post" id= "post-${post.id}">
            <h3 id = "post-title-${[post.id]}">${post.title}</h3>
            <p id = "post-body-${post.id}">${post.body}</p>
            <button onClick = "editPostHandler(${post.id})" >Edit</button>
            <button onClick = "deletePost(${index})" >Delete</button>
        </div>`
    })

    document.querySelector('#div-post-entries').innerHTML = entries
}

// function deletePost(index){
//     fetch(`https://jsonplaceholder.typicode.com/posts`)
//     .then(response => response.json())
//     .then(result => {
//         showPosts(result)
       
//        result.splice(index, 1);

//      console.log( document.querySelector(`#post-${index + 1}`));
    
//       document.querySelector(`#post-${index + 1}`).remove()
      
         
//     }
//     )
// }



function deletePost(index){
    fetch(`https://jsonplaceholder.typicode.com/posts/${index + 1}`, 
        {
            method:'DELETE',
            headers:{
                'Content-Type': 'application/json'
            }
        }
    )
  
    document.querySelector(`#post-${index + 1}`).remove()
}

// function to edit post
function editPostHandler(id){

    const title = document.querySelector(`#post-title-${id}`).innerHTML;
    const body = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#txt-edit-id').value = id;

    document.querySelector('#btn-submit-update').disabled = false;
}




document.querySelector(`#form-edit-post`).addEventListener('submit', event=>{
    event.preventDefault();

    let id = document.querySelector('#txt-edit-id').value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
        {
            method: 'PUT',
            body: JSON.stringify(
                {
                    title: document.querySelector('#txt-edit-title').value,
                    body: document.querySelector('#txt-edit-body').value,
                    id: id,
                    userId: 1
                }
            ),
            headers:{
                'Content-Type': 'application/json'
            }
        }
    )
    .then(response => response.json())
    .then(result =>{
        console.log(result);
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        document.querySelector('#txt-edit-id').value = null;

        document.querySelector('#btn-submit-update').disabled = true;

    })
})

document.querySelector('#form-add-post').addEventListener('submit', event =>{
    event.preventDefault();

    fetch(`https://jsonplaceholder.typicode.com/posts`, 
        {
            method:'POST',
            body: JSON.stringify(
                {
                    title: document.querySelector('#txt-title').value,
                    body: document.querySelector('#txt-body').value,
                    userId: 1
                }
            ),

            headers: {
                'Content-Type' : 'application/json'
            }
        }
    ).then(response=> response.json())
    .then(result => console.log(result))

    document.querySelector('#txt-title').value = null;
    document.querySelector('#txt-body').value = null;

    alert('Post is successfully added!')
})
